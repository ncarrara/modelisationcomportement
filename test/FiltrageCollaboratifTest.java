/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.File;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import fc.FiltrageCollaboratif;
import static fc.FiltrageCollaboratif.NA;
import fc.Utils;

/**
 *
 * @author Pioupiou
 */
public class FiltrageCollaboratifTest {

    final double[][] matrice = {
        {5, 3, 4, 1, NA, NA, NA},
        {5, 3, 4, 1, 5, 2, 5},
        {5, NA, 4, 1, 5, 3, NA},
        {1, 3, 2, 5, 1, 4, 2},
        {4, NA, 4, 4, 4, NA, 4}};

    private FiltrageCollaboratif fc;

    public FiltrageCollaboratifTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        fc = new FiltrageCollaboratif(matrice);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void pearson() {
//        assertEquals(1., fc.distancePearsonKmeans(1 - 1, 1 - 1), 0.00001);
        assertEquals(0.978, fc.distancePearson(1 - 1, 2 - 1), 0.00001);
//        assertEquals(0.979, fc.distancePearsonKmeans(1 - 1, 3 - 1), 0.00001);
//        assertEquals(-0.993, fc.distancePearsonKmeans(1 - 1, 4 - 1), 0.00001);
//        assertEquals(0.00, fc.distancePearsonKmeans(1 - 1, 5 - 1), 0.00001);
    }

//    @Test
//    public void fc1test(){
//        System.out.println(""+fc1.distanceCosine(0, 1));
//    }
//
//     @Test
//     public void euclienne() {
//         assertEquals(0.0,fc.distanceEuclidienne(1-1, 2-1),0.00001);
//         assertEquals(0.0,fc.distanceEuclidienne(1-1, 3-1),0.00001);
//     }
//     @Test
//     public void cosine() {
//         assertEquals(1.0,fc.distanceCosine(1-1, 2-1),0.00001);
//         assertEquals(1.0,fc.distanceCosine(1-1, 3-1),0.00001);
//         assertEquals(0.605,fc.distanceCosine(1-1, 4-1),0.00001);
//     }
}
