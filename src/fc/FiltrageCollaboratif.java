/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static fc.FiltrageCollaboratif.TypeDistance.PEARSON;
import java.util.LinkedList;

/**
 *
 * @author CARRARA Nicolas carrara1u@etu.univ-lorraine.fr
 */
public class FiltrageCollaboratif {

    /**
     * @return the nbUsers
     */
    public int getNbUsers() {
        return nbUsers;
    }

    /**
     * @return the nbItems
     */
    public int getNbItems() {
        return nbItems;
    }

    public enum TypeDistance {

        EUCLIDIENNE, COSINE, PEARSON;
    }

    public enum TypeAlgo {

        CLASSIQUE, KNN, KMEANS;
    }

    public static final byte NA = 0;
    public static final byte MAX_NOTE = 5;
    public static final byte MIN_NOTE = 1;

    /**
     * l : user c : item
     */
    private final double[][] v;

    private int nbUsers;

    private int nbItems;

    public FiltrageCollaboratif(double[][] matrice) {
        this.v = matrice;
        nbUsers = matrice.length;
        nbItems = matrice[0].length;
    }

    public static final double computeVoidPercentage(double[][] matrice) {
        double percentage = 0.;
        for (int u = 0; u < matrice.length; u++) {
            for (int i = 0; i < matrice[0].length; i++) {
                if (matrice[u][i] == NA) {
                    percentage++;
                }
            }
        }
        percentage /= (matrice.length * matrice[0].length);
        return percentage;
    }

    public static double mae(double[][] m1, double[][] m2) {
        double mae = 0.0;
        for (int i = 0; i < m1.length; i++) {
            for (int j = 0; j < m1[0].length; j++) {
                mae += Math.abs(m1[i][j] - m2[i][j]);
            }
        }
        mae /= m1.length * m1[0].length;
        return mae;
    }

    public static double maeWithoutNA(double[][] utest, double[][] upred) {
        double mae = 0.0;
        int nb = 0;
        for (int i = 0; i < utest.length; i++) {
            for (int j = 0; j < utest[0].length; j++) {
                if (utest[i][j] != NA && upred[i][j] != NA) {
                    mae += Math.abs(utest[i][j] - upred[i][j]);
                    nb++;
                }
            }
        }
        mae /= nb;
        return mae;
    }

    /**
     * @param k
     * @return
     * @throws Exception
     */
    private double[][] kmeans(int k) throws Exception {
        if (k > nbUsers || k < 1) {
            throw new Exception("k incorrect pour kmeans");
        }
//        boolean[] partitionnonvide = new boolean[k];
        double[][] predictions = new double[nbUsers][nbItems];
//        double[][] sommes = new double[k][nbItems];
//        double[][] prev_sommes = new double[k][nbItems];
        double[][] prev_moyennes = new double[k][nbItems];
        double[][] moyennes = new double[k][nbItems];
        List<Integer> alreadyPull = new ArrayList<>(k);
        int[] partition = new int[nbUsers];
        int[] prev_partition = new int[nbUsers];
        for (int u = 0; u < nbUsers; u++) {
            partition[u] = -1;
            prev_partition[u] = -1;
        }
        double[][] nbByPartition = new double[k][nbItems];
//        List<List<Integer>> lpartition = new ArrayList<>(k);
//        for (int i = 0; i < k; i++) {
//            lpartition.add(new ArrayList<>());
//        }
        /**
         * on ajoute un utilisateur au hasard par partition
         */
        for (int p = 0; p < k; p++) {
            int u = -1;
            u = (int) (Math.random() * (v.length - 1));
            do {
                // pas sur que ça donne une distrib uniforme mais ça marche bien
                u = (u + 1) % v.length;
            } while (alreadyPull.contains(u));
            alreadyPull.add(u);
            partition[u] = p;
//            partition[p] = p;
//            lpartition.get((int)p).add(u);
            prev_partition[u] = partition[u];
//            sommes[p] = Arrays.copyOf(v[u], v[u].length);
//            prev_sommes[p] = Arrays.copyOf(v[u], v[u].length);
            moyennes[p] = Arrays.copyOf(v[u], v[u].length);
            prev_moyennes[p] = Arrays.copyOf(v[u], v[u].length);
            for (int i = 0; i < nbItems; i++) {
                if (v[u][i] != NA) {
                    nbByPartition[p][i]++;
                }
            }
        }
//        System.out.println("alreadyPull : " + alreadyPull);

//        System.out.println("" + Arrays.toString(partition));
//        System.out.println("" + Arrays.toString(prev_partition));
        /**
         * on repartit chaque item dans la partition. Un item ira dans la
         * partition qui minimise la distance entre l'item et la moyenne de la
         * partition Note : on pourrait faire une moyenne pondérée en fonction
         * de la distance d'un point par rapport au centre de gravité du cluster
         */
        boolean convergence = false;
        int closestPartition = -1;
        double closestDiff;
        double diff = 0;
        int n = 0;
        double delta = Double.MAX_VALUE;

        while (!convergence && n < 15 && delta > 0.0001) {
            convergence = true;
            /**
             * on cherche la nouvelle partition pour chaque utilisateur u
             */
//            int[] part = Arrays.copyOf(partition, k);
            for (int u = 0; u < nbUsers; u++) {
//                System.out.println("----- u : "+u+" ----------");
                closestDiff = 1.1;
                /**
                 * On cherche la partition la plus proche en terme de distance
                 * entre l'utilisateur u et la moyenne du cluster
                 */
                for (int p = 0; p < k; p++) {
//                        if ((diff = distancePearsonKmeans(v[u], prev_moyennes[p])) < closestDiff) {
                    if ((diff = distanceEuclidienneKmeans(v[u], prev_moyennes[p])) < closestDiff) {
                        closestDiff = diff;
                        closestPartition = p;
                    }
//                    System.out.println("diff : " + diff);
                }
//                System.out.println("closestPartition: "+closestPartition);
                partition[u] = closestPartition;
                /**
                 * Si on change un user de partition il faut recalculer les
                 * moyennes
                 */
                if (prev_partition[u] != partition[u]) {
                    convergence = false;
                    for (int i = 0; i < nbItems; i++) {
                        if (v[u][i] != NA) {
                            moyennes[partition[u]][i]
                                    = (moyennes[partition[u]][i] * nbByPartition[partition[u]][i]
                                    + v[u][i])
                                    / (nbByPartition[partition[u]][i] + 1);
                            nbByPartition[partition[u]][i]++;
//                            if (Double.isInfinite(moyennes[partition[u]][i])) {
//                                System.exit(-2);
//                            }
                            if (prev_partition[u] > -1) {
                                if (nbByPartition[prev_partition[u]][i] > 1) {
                                    moyennes[prev_partition[u]][i]
                                            = (moyennes[prev_partition[u]][i] * nbByPartition[prev_partition[u]][i]
                                            - v[u][i])
                                            / (nbByPartition[prev_partition[u]][i] - 1.0);
                                    nbByPartition[prev_partition[u]][i]--;
                                } else {
                                    moyennes[prev_partition[u]][i] = NA;
                                    nbByPartition[prev_partition[u]][i] = 0;
                                }
                            }
                        }
                    }
                    prev_partition[u] = partition[u];

                }
            }//fin for
            int nb = 0;
            delta = 0.0;
            /**
             * On calcul delta : le critère définissant l'amélioration de la
             * solution On calcul les moyennes provisoires
             */
            for (int p = 0; p < k; p++) {
                for (int i = 0; i < nbItems; i++) {
                    if (moyennes[p][i] != NA) {
                        if (prev_moyennes[p][i] != NA) {
                            delta += Math.abs(moyennes[p][i] - prev_moyennes[p][i]);
                            nb++;
                        }
                    }
                    prev_moyennes[p][i] = moyennes[p][i];
                    if (Double.isInfinite(moyennes[p][i])) {
                        System.exit(-1);
                    }
                }
            }
            delta /= nb;
            n++;
        }

        /**
         * on remplit la matrice de prediction en utilisant les valeurs moyennes
         * des items remplies dans les différents clusters. Note : on pourrait
         * faire plus fin en prénant une valeur pour un item inconnu en
         * moyennant de manière pondérée via les similaritées avec les autres
         * users du cluster
         */
        for (int u = 0; u < nbUsers; u++) {
            for (int i = 0; i < nbItems; i++) {
                predictions[u][i] = v[u][i] == NA ? Math.floor(moyennes[partition[u]][i] + 0.5) : v[u][i];
            }
        }
        return predictions;
    }

    public double distancePearsonKmeans(double[] u1, double[] u2) {
        double moyenne1 = 0.0, moyenne2 = 0.0;
        int nb1 = 0, nb2 = 0;
        for (int i = 0; i < nbItems; i++) {
            if (u1[i] != NA) {
                moyenne1 += u1[i];
                nb1++;
            }
            if (u2[i] != NA) {
                moyenne2 += u2[i];
                nb2++;
            }
        }
        moyenne1 /= nb1;
        moyenne2 /= nb2;
        double n = 0.0, d1 = 0.0, d2 = 0.0;
        double temp1, temp2;
        for (int i = 0; i < nbItems; i++) {
            if (u1[i] != NA) {
                d1 += Math.pow((temp1 = u1[i] - moyenne1), 2.);
                if (u2[i] != NA) {
                    d2 += Math.pow((temp2 = u2[i] - moyenne2), 2.);
                    n += temp1 * temp2;
                }
            } else {
                if (u2[i] != NA) {
                    d2 += Math.pow(u2[i] - moyenne2, 2.);
                }
            }
        }
        double distance = d1 == 0 || d2 == 0
                ? 0.0 : n / (Math.sqrt(d1 * d2)); // cas où un user vote toujours avec la même note
        return (distance + 1) / 2.;
    }

    public double distanceEuclidienneKmeans(double[] u1, double[] u2) {
        double distancemax = 0.0;
        double distance = 0.0;
        for (int i = 0; i < nbItems; i++) {
            if (u1[i] != NA && u2[i] != NA) {
                distance += Math.pow(u1[i] - u2[i], 2);
                distancemax += Math.pow(MAX_NOTE - MIN_NOTE, 2);
            }
        }
        distance = Math.sqrt(distance);
//        System.out.println("distance : " + distance);
//        System.out.println("distance : " + distancemax);
        distance = distancemax == 0.0 ? 1.0 : distance / distancemax;
        return distance;
    }

    public double[][] predict(TypeDistance typeDistance, TypeAlgo typeAlgo, int k) throws Exception {
        long startTime = System.currentTimeMillis();
        double[][] prediction = new double[nbUsers][nbItems];
        if (typeAlgo == TypeAlgo.KMEANS) {
            prediction = kmeans(k);
        } else {

            double[] moyennes = new double[nbUsers];
            int[] nbs = new int[nbUsers];
            double note;
            for (int item = 0; item < nbItems; item++) {
                for (int u = 0; u < nbUsers; u++) {
                    if ((note = v[u][item]) != NA) {
                        moyennes[u] += note;
                        nbs[u]++;
                    }
                }
            }
            for (int u = 0; u < nbUsers; u++) {
                moyennes[u] /= nbs[u];
            }
            double[][] sim = null;
            switch (typeDistance) {
                case PEARSON:
                    sim = computeSimilaritiesPearsonQuick(moyennes);
                    break;
                default:
                    sim = computeSimilarities(typeDistance);
                    break;
            }
            for (int u = 0; u < nbUsers; u++) {
                for (int i = 0; i < nbItems; i++) {
                    switch (typeAlgo) {
                        case CLASSIQUE:
                            prediction[u][i] = Math.floor(predictWeighed2(u, i, sim[u], moyennes) + 0.5);
                            break;
                        case KNN:
                            prediction[u][i] = Math.floor(predictKNN(u, i, sim[u], moyennes, k) + 0.5);
                            break;
                        default:
                    }
                }
            }
        }
        long endTime = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        System.out.println("computation time : " + totalTime + "ms");
        return prediction;
    }

    public double predictSimple(int user, int item) {
        double pred;
        if ((pred = v[user][item]) == NA) {
            int nb = 0;
            pred = 0.0;
            for (int u = 0; u < nbUsers; u++) {
                pred += v[u][item];
                nb++;
            }

            pred /= nb;
            pred = pred > 5 ? 5 : pred < 1 ? 1 : pred;
        }
        return pred;
    }

    public double predictWeigthed(int user, int item, double[] sim) {
        double pred;
        if ((pred = v[user][item]) == NA) {
            double n = 0.0, d = 0.0, note;
            for (int u = 0; u < nbUsers; u++) {
                if (user != u) {
                    if ((note = v[u][item]) != NA) {
                        n += sim[u] * note;
                        d += Math.abs(sim[u]);
                    }
                }
            }
            pred = (n == 0.0 ? 0.0 : n / d);
            pred = pred > 5 ? 5 : pred < 1 ? 1 : pred;
        }

        return pred;
    }

    public double predictKNN(int user, int item, double[] sim, double[] moyennes, int k) {
        double pred;
//        System.out.println("" + Arrays.toString(sim));
        Arrays.sort(sim);
//        System.out.println("" + Arrays.toString(sim));
        if ((pred = v[user][item]) == NA) {
            double n = 0.0, d = 0.0, note;
            for (int u = sim.length - k; u < sim.length; u++) {
                if (user != u) {
                    if ((note = v[u][item]) != NA) {
                        n += sim[u] * (note - moyennes[u]);
                        d += Math.abs(sim[u]);
                    }
                }
            }
            pred = moyennes[user] + (n == 0.0 ? 0.0 : n / d);
            pred = pred > 5 ? 5 : pred < 1 ? 1 : pred;
        }

        return pred;
    }

    public double predictWeighed2(int user, int item, double[] sim, double[] moyennes) {
        double pred;
        if ((pred = v[user][item]) == NA) {
            double n = 0.0, d = 0.0, note;
            for (int u = 0; u < nbUsers; u++) {
                if (user != u) {
                    if ((note = v[u][item]) != NA) {
                        n += sim[u] * (note - moyennes[u]);
                        d += Math.abs(sim[u]);
                    }
                }
            }
            pred = moyennes[user] + (n == 0.0 ? 0.0 : n / d);
            pred = pred > 5 ? 5 : pred < 1 ? 1 : pred;
        }

        return pred;
    }

    /**
     * Note : pearson est symétrique donc la matrice doit être symétrique
     *
     * @param typeDistance
     * @return
     */
    public double[][] computeSimilarities(TypeDistance typeDistance) {
        double[][] similarities = new double[nbUsers][nbUsers];
        for (int i = 0; i < nbUsers; i++) {
            for (int j = i; j < nbUsers; j++) {
                similarities[i][j] = distanceBetweenUsers(i, j, typeDistance);
                similarities[j][i] = similarities[i][j];
            }
        }
        return similarities;
    }

    public double[][] computeSimilaritiesPearsonQuick(double[] moyennes) {
        double[][] similarities = new double[nbUsers][nbUsers];
        for (int i = 0; i < nbUsers; i++) {
            for (int j = i; j < nbUsers; j++) {
                similarities[i][j] = distancePearsonQuick(i, j, moyennes[i], moyennes[j]);
                similarities[j][i] = similarities[i][j];
            }
        }
        return similarities;
    }

    public double distanceBetweenUsers(int u1, int u2, TypeDistance typeDistance) {
        switch (typeDistance) {
            case EUCLIDIENNE:
                return distanceEuclidienne(u1, u2);
            case COSINE:
                return distanceCosine(u1, u2);
            case PEARSON:
                return distancePearson(u1, u2);
            default:
                // cas impossible à la compilation
                return Double.NaN;
            //throw new Exception("Distance "+typeDistance+" inconnue");
        }
    }

    public double distanceEuclidienne(int u1, int u2) {
        double distance = 0.0;
        for (int c = 0; c < nbItems; c++) {
            if (v[u1][c] != NA && v[u2][c] != NA) {
                distance += Math.pow(v[u1][c] - v[u2][c], 2);
            }
        }
        distance = Math.sqrt(distance);
        return distance;
    }

    /**
     * Note de performance : si NA = 0 on est pas obligé de faire des tests Cela
     * dit, dans la mesure où la matrice est presque vide, on gagne surement à
     * tester le vide au lieu de faire une multiplication inutile
     *
     * @param u1
     * @param u2
     * @return
     */
    public double distanceCosine(int u1, int u2) {
        double n = 0.0;
        double d1 = 0.0;
        double d2 = 0.0;
        for (int i = 0; i < nbItems; i++) {
            if (v[u1][i] != NA) {
                d1 += Math.pow(v[u1][i], 2.);
                if (v[u2][i] != NA) {
                    d2 += Math.pow(v[u2][i], 2.);
                    n += (v[u1][i] * v[u2][i]);
                }
            } else {
                if (v[u2][i] != NA) {
                    d2 += Math.pow(v[u2][i], 2.);
                }
            }
        }
        double distance = n / (Math.sqrt(d1 * d2));
        return distance;
    }

    public double distancePearson(int u1, int u2) {
        double moyenne1 = 0.0, moyenne2 = 0.0;
        int nb1 = 0, nb2 = 0;
        for (int i = 0; i < nbItems; i++) {
            if (v[u1][i] != NA) {
                moyenne1 += v[u1][i];
                nb1++;
            }
            if (v[u2][i] != NA) {
                moyenne2 += v[u2][i];
                nb2++;
            }
        }
        moyenne1 /= nb1;
        moyenne2 /= nb2;
        double n = 0.0, d1 = 0.0, d2 = 0.0;
        double temp1, temp2;
        for (int i = 0; i < nbItems; i++) {
            if (v[u1][i] != NA) {
                d1 += Math.pow((temp1 = v[u1][i] - moyenne1), 2.);
                if (v[u2][i] != NA) {
                    d2 += Math.pow((temp2 = v[u2][i] - moyenne2), 2.);
                    n += temp1 * temp2;
                }
            } else {
                if (v[u2][i] != NA) {
                    d2 += Math.pow(v[u2][i] - moyenne2, 2.);
                }
            }
        }
        double distance = d1 == 0 || d2 == 0
                ? 0.0 : n / (Math.sqrt(d1 * d2)); // cas où un user vote toujours avec la même note
        return distance;
    }

    public double distancePearsonQuick(int u1, int u2, double moyenne1, double moyenne2) {
        double n = 0.0, d1 = 0.0, d2 = 0.0;
        double temp1, temp2;
        for (int i = 0; i < nbItems; i++) {
            if (v[u1][i] != NA) {
                d1 += Math.pow((temp1 = v[u1][i] - moyenne1), 2.);
                if (v[u2][i] != NA) {
                    d2 += Math.pow((temp2 = v[u2][i] - moyenne2), 2.);
                    n += temp1 * temp2;
                }
            } else {
                if (v[u2][i] != NA) {
                    d2 += Math.pow(v[u2][i] - moyenne2, 2.);
                }
            }
        }
        double distance = d1 == 0 || d2 == 0
                ? 0.0 : n / (Math.sqrt(d1 * d2)); // cas où un user vote toujours avec la même note
        return distance;
    }

}
