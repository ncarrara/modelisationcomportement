/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CARRARA Nicolas carrara1u@etu.univ-lorraine.fr
 */
public class Utils {

    public final static double[][] parseMovieLens(File udata, File uinfo) {
        int nbUsers = 0;
        int nbItems = 0;
        try {
            BufferedReader brinfo = new BufferedReader(new FileReader(uinfo));
            nbUsers = Integer.parseInt(brinfo.readLine().split(" ")[0]);
            nbItems = Integer.parseInt(brinfo.readLine().split(" ")[0]);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
//        System.out.println("nbUsers : " + nbUsers);
//        System.out.println("nbItems : " + nbItems);
        int[][] tab = parseIntCSVFile(udata, "\t");
        double[][] matrice = new double[nbUsers][nbItems];
        for (int l = 0; l < tab.length; l++) {
            matrice[tab[l][0] - 1][tab[l][1] - 1] = tab[l][2];
        }
        return matrice;
    }
    
     
    
    
    public final static double[][] parseMovieLensUnionBaseAndTest(File udata, File utest, File uinfo) {
        int nbUsers = 0;
        int nbItems = 0;
        try {
            BufferedReader brinfo = new BufferedReader(new FileReader(uinfo));
            nbUsers = Integer.parseInt(brinfo.readLine().split(" ")[0]);
            nbItems = Integer.parseInt(brinfo.readLine().split(" ")[0]);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
//        System.out.println("nbUsers : " + nbUsers);
//        System.out.println("nbItems : " + nbItems);
        int[][] tab = parseIntCSVFile(udata, "\t");
        double[][] matrice = new double[nbUsers][nbItems];
        for (int l = 0; l < tab.length; l++) {
            matrice[tab[l][0] - 1][tab[l][1] - 1] = tab[l][2];
        }
        int[][] tab2 = parseIntCSVFile(utest, "\t");
        for (int l = 0; l < tab2.length; l++) {
            matrice[tab2[l][0] - 1][tab2[l][1] - 1] = tab2[l][2];
        }
        return matrice;
    }

//    public static final String SEP = ";";
    /**
     * Parse un fichier CSV. Si ce n'est pas une matrice (un triangle par
     * exemple) alors les "trous" sont remplacés automatiquement par des zéros.
     *
     * @param file
     * @return
     */
    public final static double[][] parseCSVFile(File file, String sep) {
        BufferedReader br = null;
        String line;
        List<String[]> lignes = new ArrayList<>();
        int maxWidth = 0;
        try {
            br = new BufferedReader(new FileReader(file));
            while ((line = br.readLine()) != null) {
                String[] ligne = line.split(sep);
                lignes.add(ligne);
                maxWidth = maxWidth < ligne.length ? ligne.length : maxWidth;
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }

        double[][] matrice = new double[lignes.size()][maxWidth];
        for (int l = 0; l < lignes.size(); l++) {
            if (!"".equals(lignes.get(l)[0])) {
                for (int c = 0; c < maxWidth; c++) {
                    if (c < lignes.get(l).length) {
                        matrice[l][c] = Double.parseDouble(lignes.get(l)[c]);
                    }
                }
            }

        }

        return matrice;
    }

    public final static int[][] parseIntCSVFile(File file, String sep) {
        BufferedReader br = null;
        String line;
        List<String[]> lignes = new ArrayList<>();
        int maxWidth = 0;
        try {
            br = new BufferedReader(new FileReader(file));
            while ((line = br.readLine()) != null) {
                String[] ligne = line.split(sep);
                lignes.add(ligne);
                maxWidth = maxWidth < ligne.length ? ligne.length : maxWidth;
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }

        int[][] matrice = new int[lignes.size()][maxWidth];
        for (int l = 0; l < lignes.size(); l++) {
            if (!"".equals(lignes.get(l)[0])) {
                for (int c = 0; c < maxWidth; c++) {
                    if (c < lignes.get(l).length) {
                        matrice[l][c] = Integer.parseInt(lignes.get(l)[c]);
                    }
                }
            }

        }

        return matrice;
    }

    public final static void writeCSVFile(File file, double[][] matrice, String sep) {
        Writer writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(file), "utf-8"));
            for (int l = 0; l < matrice.length; l++) {
                if (l != 0) {
                    writer.write("\n");
                }
                for (int c = 0; c < matrice[l].length - 1; c++) {
                    writer.write(matrice[l][c] + sep);
                }
                writer.write(matrice[l][matrice[l].length - 1] + "");
            }
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                writer.close();
            } catch (IOException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Print une matrice complete sur la sortie standard
     *
     * @param tableau
     */
    public static String toStringMatrice(double[][] tableau, int chiffreSignificatif) {
        String res = "";
        String format = "#0.";
        for (int i = 0; i < chiffreSignificatif; i++) {
            format += "0";

        }
        NumberFormat formatter = new DecimalFormat(format);

        for (int j = 0; j < tableau.length; j++) {
            res += "\n|";
            for (int k = 0; k < tableau[0].length; k++) {

                res += (tableau[j][k] >= 0 ? " " : "") + formatter.format(tableau[j][k]) + "|";
            }
        }
        return res;
    }

}
