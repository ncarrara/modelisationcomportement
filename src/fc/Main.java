/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fc;

import java.io.File;
import static fc.FiltrageCollaboratif.TypeAlgo.CLASSIQUE;
import static fc.FiltrageCollaboratif.TypeAlgo.KMEANS;
import static fc.FiltrageCollaboratif.TypeAlgo.KNN;
import static fc.FiltrageCollaboratif.TypeDistance.COSINE;
import static fc.FiltrageCollaboratif.TypeDistance.PEARSON;

/**
 *
 * @author CARRARA Nicolas carrara1u@etu.univ-lorraine.fr
 */
public class Main {

    public static void main(String[] args) throws Exception {
        createFilePrediction();
        compare();
        b();
        c();
        kmeansvoteclasse();
        kmeansmovielens();
    }
    static int k_kmeans_degenere = 1;
    static int k_kmeans_bis = 3;
    static int k_kmeans_bisbis = 20;
    static int k_kmeans = 50;
    static int k_knn = 3;

    private static void createFilePrediction() throws Exception {
        double[][] matrice = Utils.parseMovieLens(new File("ml-100k/u1.base"), new File("ml-100k/u.info"));
        FiltrageCollaboratif fc = new FiltrageCollaboratif(matrice);
        double[][] pearson_classique = fc.predict(PEARSON, CLASSIQUE, 0);
        Utils.writeCSVFile(new File("u1_pearson_classique.csv"), pearson_classique, " ");
        double[][] cosine_classique = fc.predict(COSINE, CLASSIQUE, 0);
        Utils.writeCSVFile(new File("u1_cosine_classique.csv"), cosine_classique, " ");
        double[][] pearson_knn = fc.predict(PEARSON, KNN, k_knn);
        Utils.writeCSVFile(new File("u1_pearson_knn_" + k_knn + ".csv"), pearson_knn, " ");
        double[][] cosine_knn = fc.predict(COSINE, KNN, k_knn);
        Utils.writeCSVFile(new File("u1_cosine_knn_" + k_knn + ".csv"), cosine_knn, " ");

        double[][] kmeansdegenere = fc.predict(null, KMEANS, k_kmeans_degenere);
        Utils.writeCSVFile(new File("u1_kmeans_" + k_kmeans_degenere + ".csv"), kmeansdegenere, " ");
        double[][] kmeansbis = fc.predict(null, KMEANS, k_kmeans_bis);
        Utils.writeCSVFile(new File("u1_kmeans_" + k_kmeans_bis + ".csv"), kmeansbis, " ");
        double[][] kmeansbisbis = fc.predict(null, KMEANS, k_kmeans_bisbis);
        Utils.writeCSVFile(new File("u1_kmeans_" + k_kmeans_bisbis + ".csv"), kmeansbisbis, " ");
        double[][] kmeans = fc.predict(null, KMEANS, k_kmeans);
        Utils.writeCSVFile(new File("u1_kmeans_" + k_kmeans + ".csv"), kmeans, " ");
    }

    private static void compare() {
        double[][] matrice1 = Utils.parseMovieLens(new File("ml-100k/u1.base"), new File("ml-100k/u.info"));
        System.out.println("vide au départ : " + FiltrageCollaboratif.computeVoidPercentage(matrice1) * 100 + "%");

        double[][] pearson_classique = Utils.parseCSVFile(new File("u1_pearson_classique.csv"), " ");
        double[][] cosine_classique = Utils.parseCSVFile(new File("u1_cosine_classique.csv"), " ");
        double[][] pearson_knn = Utils.parseCSVFile(new File("u1_pearson_knn_" + k_knn + ".csv"), " ");
        double[][] cosine_knn = Utils.parseCSVFile(new File("u1_cosine_knn_" + k_knn + ".csv"), " ");
        double[][] kmeansbis = Utils.parseCSVFile(new File("u1_kmeans_" + k_kmeans_bis + ".csv"), " ");
        double[][] kmeansbisbis = Utils.parseCSVFile(new File("u1_kmeans_" + k_kmeans_bisbis + ".csv"), " ");
        double[][] kmeans = Utils.parseCSVFile(new File("u1_kmeans_" + k_kmeans + ".csv"), " ");
        double[][] kmeansdegenere = Utils.parseCSVFile(new File("u1_kmeans_" + k_kmeans_degenere + ".csv"), " ");

        double[][] utest = Utils.parseMovieLens(new File("ml-100k/u1.test"), new File("ml-100k/u.info"));
        System.out.println("mae(utest,pearson_classique) : " + FiltrageCollaboratif.maeWithoutNA(utest, pearson_classique));
        System.out.println("vide : " + FiltrageCollaboratif.computeVoidPercentage(pearson_classique) * 100 + "%");
        System.out.println("mae(utest,cosine_classique) : " + FiltrageCollaboratif.maeWithoutNA(utest, cosine_classique));
        System.out.println("vide : " + FiltrageCollaboratif.computeVoidPercentage(cosine_classique) * 100 + "%");
        System.out.println("mae(utest,pearson_knn) with k =" + k_knn + " : " + FiltrageCollaboratif.maeWithoutNA(utest, pearson_knn));
        System.out.println("vide : " + FiltrageCollaboratif.computeVoidPercentage(pearson_knn) * 100 + "%");
        System.out.println("mae(utest,cosine_knn) with k =" + k_knn + " : " + FiltrageCollaboratif.maeWithoutNA(utest, cosine_knn));
        System.out.println("vide : " + FiltrageCollaboratif.computeVoidPercentage(cosine_knn) * 100 + "%");
        System.out.println("mae(utest,kmeans) with k =" + k_kmeans_degenere + " : " + FiltrageCollaboratif.maeWithoutNA(utest, kmeansdegenere));
        System.out.println("vide : " + FiltrageCollaboratif.computeVoidPercentage(kmeansdegenere) * 100 + "%");
        System.out.println("mae(utest,kmeans) with k =" + k_kmeans_bis + " : " + FiltrageCollaboratif.maeWithoutNA(utest, kmeansbis));
        System.out.println("vide : " + FiltrageCollaboratif.computeVoidPercentage(kmeansbis) * 100 + "%");
        System.out.println("mae(utest,kmeans) with k =" + k_kmeans_bisbis + " : " + FiltrageCollaboratif.maeWithoutNA(utest, kmeansbisbis));
        System.out.println("vide : " + FiltrageCollaboratif.computeVoidPercentage(kmeansbisbis) * 100 + "%");
        System.out.println("mae(utest,kmeans) with k =" + k_kmeans + " : " + FiltrageCollaboratif.maeWithoutNA(utest, kmeans));
        System.out.println("vide : " + FiltrageCollaboratif.computeVoidPercentage(kmeans) * 100 + "%");

    }

    private static void b() throws Exception {
        double[][] matrice = Utils.parseCSVFile(new File("votes-films-2015.csv"), ";");
        FiltrageCollaboratif fc = new FiltrageCollaboratif(matrice);
        double[][] predictions = fc.predict(PEARSON, CLASSIQUE, 0);
        Utils.writeCSVFile(new File("votes-films-2015_pred.csv"), predictions, " ");
        double[][] matrice_max = Utils.parseCSVFile(new File("maxime_pred.txt"), "  ");

        System.out.println(FiltrageCollaboratif.mae(predictions, matrice_max));

        double[][] diff = new double[matrice.length][matrice[0].length];
        for (int i = 0; i < matrice_max.length; i++) {
            for (int j = 0; j < matrice_max[0].length; j++) {
                diff[i][j] = matrice_max[i][j] - predictions[i][j];

            }
        }
        System.out.println(Utils.toStringMatrice(diff, 5));
        Utils.writeCSVFile(new File("diff_with_max.csv"), diff, " ");
    }

    private static void c() throws Exception {
        double[][] matrice = Utils.parseCSVFile(new File("votes-films-2015.csv"), ";");
        FiltrageCollaboratif fc = new FiltrageCollaboratif(matrice);
        double[][] predictions = fc.predict(null, KMEANS, 2);
        //System.out.println(""+Utils.toStringMatrice(predictions, 2));
        double[][] matrice_max = Utils.parseCSVFile(new File("maxime_pred.txt"), "  ");
        System.out.println("" + FiltrageCollaboratif.maeWithoutNA(predictions, matrice_max));
    }

    private static void kmeansvoteclasse() throws Exception {
        double[][] matrice = Utils.parseCSVFile(new File("votes-films-2015.csv"), ";");
        FiltrageCollaboratif fc = new FiltrageCollaboratif(matrice);
        double[][] matrice_max = Utils.parseCSVFile(new File("maxime_pred.txt"), "  ");
        int[] ks = {1, 2, 3, 4, 5, 6, 7};
//        int[] ks = {3/*, 5*/};
        for (int k : ks) {
            double[][] predict = fc.predict(null, KMEANS, k);
            System.out.println("--------\n" + k + " : " + FiltrageCollaboratif.maeWithoutNA(
                    matrice_max,
                    predict));
            System.out.println("vide : " + FiltrageCollaboratif.computeVoidPercentage(predict) + "%");
        }
    }

    private static void kmeansmovielens() throws Exception {
        double[][] base = Utils.parseMovieLens(new File("ml-100k/u1.base"), new File("ml-100k/u.info"));
        System.out.println("vide au départ : " + FiltrageCollaboratif.computeVoidPercentage(base) * 100 + "%");
        FiltrageCollaboratif fc = new FiltrageCollaboratif(base);
        double[][] test = Utils.parseMovieLens(new File("ml-100k/u1.test"), new File("ml-100k/u.info"));
//        int[] ks = {1, 2, 3, 4, 5, 6, 7};
////        int[] ks = {(fc.getNbUsers())};
        int[] ks = {1,3, 20, 50,500};
//        int[] ks = {3};
        int bestk = -1;
        double bestmae = 2;
//        for (int k = 1; k < 100; k++) {
        for (int k : ks) {
            System.out.println("--------- " + k + " ------------");
            double[][] predict = fc.predict(null, KMEANS, k);
            double mae = FiltrageCollaboratif.maeWithoutNA(
                    test,
                    predict);
            System.out.println("mae : " + mae);
            System.out.println("vide : " + FiltrageCollaboratif.computeVoidPercentage(predict) * 100 + "%");
            if (mae < bestmae) {
                bestmae = mae;
                bestk = k;
                System.out.println("best mae so far : " + bestmae);
                System.out.println("best k so far: " + bestk);
            }
        }

        System.out.println("best mae : " + bestmae);
        System.out.println("best k : " + bestk);
    }
}
